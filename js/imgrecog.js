//var bytes = Crypto.util.base64ToBytes(localStorage.getItem('object'));
//var string = Crypto.charenc.Binary.bytesToString(bytes);
//obj = JSON.parse(string);

obj.longtext, obj.eqname, obj.material;
obj.inEquips = false;
obj.reload = false;
obj.tkn = null;
obj.funcLocs = null;
obj.qmnum = 10000400;
obj.data = null;

/**
 * function for getting object position
 * @param substrings
 * @returns
 */
function getObjectPosition(substrings) {
	var number = null;
	for(var i = 0; i < substrings.length; i++) {
		var tmp = parseInt(substrings[i]);
		if(isNaN(tmp)) {
			continue;
		} else {
			number = tmp;
			break;
		}
	}
	return number;
}

function setDataset() {
	sap.ui.getCore().byId('application-MasterBtn').setVisible(true);
	var data = {'d': {
		'Funcloc':'RS-100', 'Functlocation':'RS-100', 'HeliApplication':'TGV-Duplex', 'HeliAtwe':'82,5',
		'HeliAtweUnit':'t', 'HeliCms2':'278', 'HeliCms2Unit':'t', 'HeliCms3':'8,8', 'Descript':'',
		'HeliCms3Unit':'MW', 'maxSpeed':'320', 'maxSpeedUnit':'km/h', 'HeliConc':'No options', 'HeliImma':'4417', 'LabelSyst':'', 'Maintplant':'1000',
		'Plangroup':'', 'Planplant':'1000', 'Sortfield':'',
		'SubFuncLocs': { 'results': [
		        {'Category': 'RS', 'Descript': 'Environmental Control', 'Funcloc': 'RS-100-021', 'Functlocation': 'RS-100-021', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Communication', 'Funcloc': 'RS-100-023', 'Functlocation': 'RS-100-023', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Electrical Power', 'Funcloc': 'RS-100-024', 'Functlocation': 'RS-100-024', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Equipment', 'Funcloc': 'RS-100-025', 'Functlocation': 'RS-100-025', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Fuel', 'Funcloc': 'RS-100-028', 'Functlocation': 'RS-100-028', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Hydraulics', 'Funcloc': 'RS-100-029', 'Functlocation': 'RS-100-029', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Indicating and recording systems', 'Funcloc': 'RS-100-031', 'Functlocation': 'RS-100-031', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Landing gear complete', 'Funcloc': 'RS-100-032', 'Functlocation': 'RS-100-032', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Lights', 'Funcloc': 'RS-100-033', 'Functlocation': 'RS-100-033', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Navigation', 'Funcloc': 'RS-100-034', 'Functlocation': 'RS-100-034', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Systems Integration and Display', 'Funcloc': 'RS-100-046', 'Functlocation': 'RS-100-046', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Doors', 'Funcloc': 'RS-100-052', 'Functlocation': 'RS-100-052', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Fuselage', 'Funcloc': 'RS-100-053', 'Functlocation': 'RS-100-053', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Nacelles/Pylons', 'Funcloc': 'RS-100-054', 'Functlocation': 'RS-100-054', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Stabilizers', 'Funcloc': 'RS-100-055', 'Functlocation': 'RS-100-055', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Windows', 'Funcloc': 'RS-100-056', 'Functlocation': 'RS-100-056', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Main Rotor', 'Funcloc': 'RS-100-062', 'Functlocation': 'RS-100-062', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Main Rotor Drives', 'Funcloc': 'RS-100-063', 'Functlocation': 'RS-100-063', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Tail Rotor', 'Funcloc': 'RS-100-064', 'Functlocation': 'RS-100-064', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Tail Rotor Drive', 'Funcloc': 'RS-100-065', 'Functlocation': 'RS-100-065', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Rotor Flight Control', 'Funcloc': 'RS-100-067', 'Functlocation': 'RS-100-067', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Power Plant', 'Funcloc': 'RS-100-071', 'Functlocation': 'RS-100-071', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Engine', 'Funcloc': 'RS-100-072', 'Functlocation': 'RS-100-072', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Engine Indication', 'Funcloc': 'RS-100-077', 'Functlocation': 'RS-100-077', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Exhaust', 'Funcloc': 'RS-100-078', 'Functlocation': 'RS-100-078', 'Supfloc': 'RS-100', 'Maintplant': '1000'},
		        {'Category': 'RS', 'Descript': 'Oil', 'Funcloc': 'RS-100-079', 'Functlocation': 'RS-100-079', 'Supfloc': 'RS-100', 'Maintplant': '1000'}
		        ]},
		'MesPnts': { 'results': [
			    {'Psort':'AC_CYCLES_COUNTER', 'Pttxt': 'Number of operating cycles', 'Point':'2', 'MesDocs': { 'results': [{'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'***', 'Cntrr':'3.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'***', 'Cntrr':'5.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H20M37S', 'Recdu':'***', 'Cntrr':'1.1000000000000000E+01'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H22M04S',  'Recdu':'***', 'Cntrr':'1.5000000000000000E+01'}]}},
			    {'Psort':'AC_HOURS_COUNTER', 'Pttxt':'Number of operating hours', 'Point':'3', 'MesDocs': { 'results': [{'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'H', 'Cntrr':'2.5200000000000000E+04'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'H', 'Cntrr':'5.4000000000000000E+04'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H20M37S', 'Recdu':'H', 'Cntrr':'9.0000000000000000E+04'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H22M04S', 'Recdu':'H', 'Cntrr':'1.0080000000000000E+05'}]}},
			    {'Psort':'ENG_CYCLES_COUNTER', 'Pttxt':'Number of engine cycles', 'Point':'5', 'MesDocs': { 'results': [{'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'***', 'Cntrr':'3.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'***', 'Cntrr':'6.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H20M37S', 'Recdu':'***', 'Cntrr':'1.3000000000000000E+01'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H22M04S', 'Recdu':'***', 'Cntrr':'1.7000000000000000E+01'}]}},
			    {'Psort':'FUEL_LEVEL_COUNTER', 'Pttxt':'Actual fuel on board', 'Point':'196', 'MesDocs': { 'results': [{'Erdat':'/Date(1414022400000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'L', 'Cntrr':'0.0000000000000000E+00'}, {'Erdat':'/Date(1414022400000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'L', 'Cntrr':'0.0000000000000000E+00'}]}},
			    {'Psort':'ROT_HOURS_COUNTER', 'Pttxt':'Number of engine hours', 'Point':'4', 'MesDocs': { 'results': [{'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'H', 'Cntrr':'2.5200000000000000E+04'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'H', 'Cntrr':'5.7600000000000000E+04'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H20M37S', 'Recdu':'H', 'Cntrr':'9.7200000000000000E+04'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H22M04S', 'Recdu':'H', 'Cntrr':'1.0800000000000000E+05'}]}},
			    {'Psort':'SLING_CYCLES_COUNTER', 'Pttxt':'Number of sling cycles', 'Point':'7', 'MesDocs': { 'results': [{'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'***', 'Cntrr':'1.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'***', 'Cntrr':'2.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H20M37S', 'Recdu':'***', 'Cntrr':'5.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H22M04S', 'Recdu':'***', 'Cntrr':'6.0000000000000000E+00'}]}},
			    {'Psort':'WYNCH_CYCLES_COUNTER', 'Pttxt':'Number of wynch cycles', 'Point':'6', 'MesDocs': { 'results': [{'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H11M49S', 'Recdu':'***', 'Cntrr':'0.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H12M30S', 'Recdu':'***', 'Cntrr':'3.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H20M37S', 'Recdu':'***', 'Cntrr':'5.0000000000000000E+00'}, {'Erdat':'/Date(1413504000000)/', 'Eruhr':'PT12H22M04S', 'Recdu':'***', 'Cntrr':'7.0000000000000000E+00'}]}}]},
		'Notifs':{ 'results': [
		        {'Tplnr' : 'RS-100', 'Equnr' : '9000051', 'Eqname' : 'Fuel', 'Qmtxt' : 'Distribution problem', 'Erdat' : '04.01.2015','Erzeit' : '13:50', 'longTxt' : 'This is a Longtext', 'Priok' : 'high','Qmnum' : '10000400'}]},
		'Orders':{ 'results': [
		        {'Abcindic': '', 'ActualFinishTime': 'PT00H00M00S', 'ActualReleaseDate': '/Date(1413763200000)/', 'ActualStartDate': 'null', 'ActualStartTime': 'PT00H00M00S', 'AddrNo': '', 'Assembly': '', 'AssemblyExternal': '', 'AssemblyGuid': '', 'AssemblyVersion': '', 'AssetNo': '', 'AssmblDesc': '', 'BasicFin': 'PT24H00M00S',
		         'Basicstart': 'PT00H00M00S', 'BillingForm': '', 'BusArea': '0001', 'CalcMotive': '', 'ChangeDate': '/Date(1413763200000)/', 'ChangedBy': 'PROEN', 'City': '', 'Client': '216', 'CoArea': '1000', 'CompCode': '1000', 'ConfirmedFinishDate': 'null', 'Costcenter': '', 'Country': '', 'Countryiso': '', 'CstgSheet': '', 'Currency': 'BRL',
		         'CurrencyIso': 'BRL', 'Customer': '', 'DeletionFlag': '', 'Deviceid': '', 'DistrChan': '', 'District': '', 'Division': '', 'EndPoint': '', 'EnterDate': '/Date(1413504000000)/', 'EnteredBy': 'DJO', 'Equidescr': 'Helicopter Complete', 'Equipment': '9000039', 'EstimatedCosts': '0.00', 'FinishDate': 'null', 'FirstOffsetTypeCode': '', 'FirstOffsetTypeCodeName': '',
		         'FirstOffsetUnit': '', 'FirstOffsetUnitIso': '', 'FirstOffsetValue': '', 'Funcldescr': 'Helicopter complete', 'Funcloc': 'RS-100', 'GroupCounter': '', 'HistoryDate': 'null', 'LeadingOrderid': '', 'LinearLength': '', 'LinearUnit': '', 'LinearUnitIso': '', 'LongTextFlag': '', 'Maintitem': '', 'Maintloc': '', 'Maintplan': '', 'Maintplant': '1000', 'Maintroom': '',
		         'MarkerDistanceEndPoint': '', 'MarkerDistanceStartPoint': '', 'MarkerDistanceUnit': '', 'MarkerDistanceUnitIso': '', 'MarkerEndPoint': '', 'MarkerStartPoint': '', 'Material': '1200-1001514-AA', 'MaterialExternal': '', 'MaterialGuid': '', 'MaterialVersion': '', 'MatlDesc': 'Wheelset Axle', 'MatMenge':'1', 'MatEinheit':'pcs', 'MnWkCtr': 'DISCR_MF', 'MnWkPlant': '1000', 'MrpRelevant': '3', 'NameList': '', 'NotifNo': '10000001',
		         'ObjectNo': 'OR000004000000', 'ObjectlistNo': '1367', 'OrderType': 'PM01', 'Orderid': '4000000', 'Ordplanid': '', 'OverheadKey': '', 'Pagestatus': '', 'Phase': '2', 'PlanPlannerGroup': '', 'Plangroup': '', 'PlannedDownTime': '0.00', 'PlannerGroup': '', 'Planplant': '1000', 'Plant': '1000', 'Plsectn': '', 'Pmacttype': '001', 'PostlCod1': '', 'PrioDesc': '', 'Priority': '',
		         'Priotype': 'PM', 'ProductionFinishDate': '/Date(1413504000000)/', 'ProductionFinishTime': 'PT07H00M00S', 'ProductionStartDate': '/Date(1413504000000)/', 'ProductionStartTime': 'PT07H00M00S', 'ProfitCtr': 'YB110', 'ProfitSegmNo': '0000000000', 'PurchDate': 'null', 'PurchNoC': '', 'Refdate': '/Date(1413504000000)/', 'Reftime': 'PT23H59M59S', 'Region': '', 'RespPlannerGroup': '',
		         'Respcctr': '1301', 'Revision': '', 'SStatus': 'FREI KKMP NMVP NTER', 'SalesDocNumber': '', 'SalesGrp': '', 'SalesItmNumber': '000000', 'SalesOff': '', 'Salesorg': '', 'SecondOffsetTypeCode': '', 'SecondOffsetTypeCodeName': '', 'SecondOffsetUnit': '', 'SecondOffsetUnitIso': '', 'SecondOffsetValue': '', 'Serialno': '', 'ServcieUom': '', 'ServcieUomIso': '', 'ServiceMaterial': '',
		         'ServiceMaterialExternal': '', 'ServiceMaterialGuid': '', 'ServiceMaterialVersion': '', 'ServiceMatlDesc': '', 'ServiceMatlQuantity': '0.000', 'ShortText': 'Bogie malfunction', 'Sortfield': '', 'StartDate': '/Date(1413504000000)/', 'StartPoint': '', 'Street': '', 'SubNumber': '', 'SuperiorActivity': '', 'SuperiorNetwork': '', 'SuperiorOrderid': '', 'Systcond': '', 'SystemAvailableFromDate': 'null',
		         'SystemAvailableFromTime': 'PT00H00M00S', 'SystemAvailableToDate': 'null', 'SystemAvailableToTime': 'PT00H00M00S', 'SystemResp': '', 'TaskListGroup': '', 'Tel1Numbr': '', 'TotalCostsAct': '0.00', 'TotalCostsPlan': '0.00', 'TotalRevenuesAct': '0.00', 'TotalRevenuesPlan': '0.00', 'TotalSettlementCosts': '0.00', 'TotalSumAct': '0.00', 'TotalSumPlan': '0.00', 'UStatus': '', 'WbsElement': '', 'WbsElementHead': '', 'WorkCntr': '',
		         'Operas': { 'results': [
		                  {'Activity': '0010', 	'Acttype': '', 'ActualFinDate': 'null',	'ActualFinTime': 'PT00H00M00S',	'ActualStartDate': 'null', 'ActualStartTime': 'PT00H00M00S', 'Assembly': '', 'AssemblyDesc': '', 'AssemblyExternal': '', 'AssemblyGuid': '', 'AssemblyVersion': '', 'CalculationKey': '1', 'Client': '216', 'ConfNo': '0000000001', 'ConstraintFinDate': 'null', 'ConstraintFinTime': 'PT00H00M00S', 'ConstraintStartDate': 'null', 'ConstraintStartTime': 'PT00H00M00S', 'ConstraintTypeFinish': '', 'ConstraintTypeStart': '', 'ControlKey': 'PM01',
		                   'CostElement': '', 'Counter': '00000001', 'Currency': '', 'CurrencyIso': '', 'Description': 'Error Helicopter', 'Deviceid': '', 'DurationMinimum': '0.0', 'DurationMinimumUnit': '', 'DurationMinimumUnitIso': '', 'DurationNormal': '3.0', 'DurationNormalUnit': 'RS', 'DurationNormalUnitIso': '', 'EarlSchedFinishDate': '/Date(1413504000000)/', 'EarlSchedFinishTime': 'PT10H22M30S', 'EarlSchedStartDate': '/Date(1413504000000)/', 'EarlSchedStartTime': 'PT07H00M00S', 'EndPoint': '', 'Equidescr': 'Helicopter Complete', 'Equipment': '9000039',
		                   'ExecFactor': '1', 'FirstOffsetTypeCode': '', 'FirstOffsetTypeCodeName': '', 'FirstOffsetUnit': '', 'FirstOffsetUnitIso': '', 'FirstOffsetValue': '', 'FreeFloat': '0', 'Funcldescr': 'Helicopter complete', 'Funcloc': 'RS-100', 'GrRcpt': '', 'InfoRec': '', 'LateSchedFinDate': '/Date(1413417600000)/', 'LateSchedFinTime': 'PT16H00M00S', 'LateSchedStartDate': '/Date(1413417600000)/', 'LateSchedStartTime': 'PT12H37M30S', 'LinearLength': '', 'LinearUnit': '', 'LinearUnitIso': '', 'LocPlant': '1000', 'Location': '', 'LongtextIndic': '',
		                   'MarkerDistanceEndPoint': '', 'MarkerDistanceStartPoint': '', 'MarkerDistanceUnit': '', 'MarkerDistanceUnitIso': '', 'MarkerEndPoint': '', 'MarkerStartPoint': '', 'Material': '1200-1001514-AA', 'MaterialExternal': '', 'MaterialGuid': '', 'MaterialVersion': '', 'MatlDesc': '', 'MatlGroup': '', 'NextPlanDate': 'null', 'NoOfConfirmationSlips': '000', 'NoOfTimeTickets': '0', 'NotifNo': '', 'NumberOfCapacities': '1', 'OrderType': 'PM01', 'PercentOfWork': '0', 'PersonN': '00000000', 'Plangroup': '', 'Planplant': '1000', 'Plant': '1000',
		                   'PreqItem': '00000', 'PreqNo': '', 'Price': '0.00', 'PriceUnit': '0', 'Priority': '', 'Priotype': 'PM', 'PriotypeDesc': '', 'PurGroup': '', 'PurchOrg': '', 'RefDate': '/Date(1413504000000)/', 'RespPlanner': '', 'RoutingNo': '1000000001', 'SStatus': 'FREI', 'SecondOffsetTypeCode': '', 'SecondOffsetTypeCodeName': '', 'SecondOffsetUnit': '', 'SecondOffsetUnitIso': '', 'SecondOffsetValue': '', 'Serialno': '', 'Sortfield': '', 'Split': '0', 'StandardTextKey': '', 'StartPoint': '', 'SubActivity': '', 'Suitability': '', 'Systcond': '', 'TotalFloat': '0',
		                   'UStatus': '', 'UnWork': 'RS', 'UnWorkIso': '', 'UnloadPt': '', 'VendorNo': '', 'Wagegroup': '', 'Wagetype': '', 'WorkActivity': '3.0', 'WorkActual': '0.000', 'WorkCntr': 'DISCR_MF'}]}}]},
		'Equis':{ 'results': [
                {'Equnr':'9000000', 'HequiShtxt':'Driven systems (passive)'}, {'Equnr':'9000001', 'HequiShtxt':'Running Gear'}, {'Equnr':'9000002', 'HequiShtxt':'Roof'},
                {'Equnr':'9000003', 'HequiShtxt':'External additions'}, {'Equnr':'9000004', 'HequiShtxt':'Carrying Bogie (Front)', 'Matnr':'1200-1009422-AA'}, {'Equnr':'9000005', 'HequiShtxt':'Indication', 'Matnr':'1200-1009458-AA'},
		        {'Equnr':'9000039', 'HequiShtxt':'Vehicle Body'}, {'Equnr':'9000040', 'HequiShtxt':'Wheelset'}, {'Equnr':'9000041', 'HequiShtxt':'Storage'},
		        {'Equnr':'9000042', 'HequiShtxt':'Power supply controls'}, {'Equnr':'9000050', 'HequiShtxt':'Distribution'}, {'Equnr':'9000051', 'HequiShtxt':'Carrying Bogie (Rear)'},
		        {'Equnr':'9000052', 'HequiShtxt':'Driving and brake controls'}, {'Equnr':'9000053', 'HequiShtxt':'Brake'}, {'Equnr':'9000054', 'HequiShtxt':'Mechanical brake force transmission'},
		        {'Equnr':'10000001', 'HequiShtxt':'Battery device'}]}
	}};
	obj.data = data;
	obj.tplnr = 'RS-100';

	// set into localstorage
	var dataset = JSON.stringify(obj.data);
	localStorage.setItem("object", dataset);

}

var crazyArchitectWorld = {
	loaded: false,
	rotating: false,
	tracker: null,
	trackables : new Array(),
	//tilesVisible : true,
	//equiTrackable : null,

	init: function initFn() {
		// To receive a notification once the image target is inside the field of vision the onEnterFieldOfVision trigger of the AR.Trackable2DObject is used. In the example the function appear() is attached. Within the appear function the previously created AR.AnimationGroup is started by calling its start() function which plays the animation once.
		// To add the AR.ImageDrawable to the image target together with the 3D model both drawables are supplied to the AR.Trackable2DObject.
		this.tracker = new AR.Tracker("assets/marenco.wtc", {
			//onLoaded: this.loadingStep
		});
		AR.context.services.sensors = false;
		this.getFuncLocs();
	},
	initTracker: function (funcLoc) {
		this.trackables[funcLoc.Functlocation] = new AR.Trackable2DObject(this.tracker, funcLoc.Functlocation, {
				// initialise cam element
			    drawables: {
			        cam: null
			    },
				onEnterFieldOfVision: function() {
					// Lesen des Technischen Platzes sofern nicht bereits gelesen
					if (obj.data === null ||  obj.data.d === undefined || obj.data.d.Functlocation != funcLoc.Functlocation){
						crazyArchitectWorld.getSubFuncLocs(funcLoc.Functlocation);

					}
					// Sofern Model VideoDrawable ist dann soll dieses weiterabgespielt werden
					if( crazyArchitectWorld.model instanceof AR.VideoDrawable ){
						crazyArchitectWorld.model.resume( );
					}
				},
				onExitFieldOfVision: function() {
					// Pause bei VideoDrawables
					if( crazyArchitectWorld.model instanceof AR.VideoDrawable ){
						crazyArchitectWorld.model.pause( );
					}
				}
			});
	},

	removeModel: function(funcLoc){
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && this.model != null ){
			this.trackables[funcLoc.Functlocation].drawables.removeCamDrawable(this.model);
			this.model = null;
		}
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && ( this.filled != 'undefined' || this.filled != null ) && this.filled instanceof AR.ImageDrawable){
			this.filled.opacity = 0;
		}

	},
	showVideo: function( funcLoc ){
		this.removeModel(funcLoc);
		this.model = new AR.VideoDrawable("assets/assemble_bogie_comp.mp4", 0.5, {
		    offsetX: 0.2,
		    offsetY: 0.2,
		    zOrder: 3
		});
		this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.model);
		this.model.play();
	},
	showOutLine: function( funcLoc ) {
		this.outLine = new AR.ImageDrawable(new AR.ImageResource("assets/" + funcLoc.Functlocation + "_outline.png"), 1, { zOrder: 0} );
		this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.outLine);
	},
	removeOutLine: function( funcLoc ){
//		Remove Outline
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && this.outLine != null ){
			this.trackables[funcLoc.Functlocation].drawables.removeCamDrawable(this.outLine);
			this.outLine = null;
		}
//		Remove Filled Image
		if( this.trackables[funcLoc.Functlocation].drawables.cam != null && this.filled != null ){
			this.trackables[funcLoc.Functlocation].drawables.removeCamDrawable(this.filled);
			this.filled = null;
		}
	},
	showMaterial: function(funcLoc) {
		this.removeModel(funcLoc);
//		Set Filled
		if( ( this.filled != 'undefined' || this.filled != null ) && this.filled instanceof AR.ImageDrawable){
			this.filled.opacity = 1;
		}else{
			this.filled = new AR.ImageDrawable(new AR.ImageResource("assets/" + funcLoc.Functlocation + "_filled.png"), 1, {zOrder: 1} );
			this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.filled);
		}
//		Create Model
		var position = getObjectPosition(obj.selPath.split("/"));
		this.model = new AR.Model("assets/"+ obj.data.d.Equis.results[position].Matnr +".wt3", {
			//	The drawables are made clickable by setting their onClick triggers. Click triggers can be set in the options of the drawable when the drawable is created. Thus, when the 3D model onClick: this.toggleAnimateModel is set in the options it is then passed to the AR.Model constructor. Similar the button's onClick: this.toggleAnimateModel trigger is set in the options passed to the AR.ImageDrawable constructor. toggleAnimateModel() is therefore called when the 3D model or the button is clicked.
			//	Inside the toggleAnimateModel() function, it is checked if the animation is running and decided if it should be started, resumed or paused.
			onClick: this.toggleAnimateModel,
			scale: {
				x: 0.40,
				y: 0.40,
				z: 0.40
			},
			translate: {
				x: -0.1,
				y: -0.31,
				z: 0
			},
			rotate: {
				tilt : 270,
				heading: 260,
				roll: -18
			},
			zOrder: 2
		});
		console.log("assets/"+ obj.data.d.Equis.results[position].Matnr +".wt3");
		this.trackables[funcLoc.Functlocation].drawables.addCamDrawable(this.model);
		// As a next step, an appearing animation is created. For more information have a closer look at the function implementation.
		this.appearingAnimation = this.createAppearingAnimation(this.model, 0.015);
		// The rotation animation for the 3D model is created by defining an AR.PropertyAnimation for the rotate.roll property.
		this.rotationAnimation =  new AR.PropertyAnimation( this.model, "rotate.heading", 0, 359, 10000 );
		this.appear();
	},

	createAppearingAnimation: function createAppearingAnimationFn(model, scale) {
		//	The animation scales up the 3D model once the target is inside the field of vision. Creating an animation on a single property of an object is done using an AR.PropertyAnimation. Since the car model needs to be scaled up on all three axis, three animations are needed. These animations are grouped together utilizing an AR.AnimationGroup that allows them to play them in parallel.
		//	Each AR.PropertyAnimation targets one of the three axis and scales the model from 0 to the value passed in the scale variable. An easing curve is used to create a more dynamic effect of the animation.
		var sx = new AR.PropertyAnimation(model, "scale.x", 0, scale, 2000, {
			type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
		});
		var sy = new AR.PropertyAnimation(model, "scale.y", 0, scale, 2000, {
			type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
		});
		var sz = new AR.PropertyAnimation(model, "scale.z", 0, scale, 2000, {
			type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_ELASTIC
		});

		return new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, [sx, sy, sz]);
	},

	appear: function appearFn() {
		// Resets the properties to the initial values.
		crazyArchitectWorld.resetModel();
		crazyArchitectWorld.appearingAnimation.start();
		this.toggleAnimateModel( );
	},

	resetModel: function resetModelFn() {
		crazyArchitectWorld.rotationAnimation.stop();
		crazyArchitectWorld.rotating = false;
		crazyArchitectWorld.model.rotate.roll = -25;
	},

	toggleAnimateModel: function toggleAnimateModelFn() {
		if (!crazyArchitectWorld.rotationAnimation.isRunning()) {
			if (!crazyArchitectWorld.rotating) {
				// Starting an animation with .start(-1) will loop it indefinitely.
				crazyArchitectWorld.rotationAnimation.start(-1);
				crazyArchitectWorld.rotating = true;
			} else {
				// Resumes the rotation animation
				crazyArchitectWorld.rotationAnimation.resume();
			}
		} else {
			// Pauses the rotation animation
			crazyArchitectWorld.rotationAnimation.pause();
		}

		return false;
	},
	// Lesen des obersten technischen Platzes
	getFuncLocs:function getFuncLocsFn(){

		var FuncLocs = {'d': {
			'Category': 'RS', 'Descript': 'Helicopter', 'Equis': {'results':[]}, 'Funcloc': 'TRAIN', 'Functlocation': 'TRAIN', 'HeliApplication': '', 'HeliAtwe': '', 'HeliAtweUnit': '', 'HeliCms2': '', 'HeliCms2Unit': '',
			'HeliCms3': '', 'HeliCms3Unit': '', 'HeliConc': '', 'HeliImma': '', 'LabelSyst': '', 'Maintplant': '', 'MesPnts': {'results':[]}, 'Notifs': {'results':[]}, 'Orders': {'results':[]}, 'Plangroup': '', 'Planplant': '',
			'Sortfield': '', 'Strind': 'HELI', 'Supfloc': '', 'SubFuncLocs': {'results':[
			     {'Category': 'RS', 'Descript': 'TGV-Duplex', 'Equis': {'results':[]}, 'Funcloc': 'RS-100', 'Functlocation': 'RS-100', 'HeliApplication': 'TGV-Duplex', 'HeliAtwe': '82,5', 'HeliAtweUnit': 't',
			      'HeliCms2': '278', 'HeliCms2Unit': 't', 'HeliCms3': '8,8', 'HeliCms3Unit': 'MW', 'HeliConc': 'No options', 'HeliImma': '4417', 'LabelSyst': '', 'Maintplant': '1000', 'MesPnts': {'results':[]}, 'Notifs': {'results':[]},
			      'Orders': {'results':[]}, 'Plangroup': '', 'Planplant': '1000', 'Sortfield': '', 'Strind': 'TRAIN', 'SubFuncLocs': {'results':[]}, 'Supfloc': 'TRAIN'}]}}};

		obj.funcLocs = FuncLocs;
		obj.position = "tiles";

		obj.funcLocs.d.SubFuncLocs.results.forEach(function(funcLoc){
			crazyArchitectWorld.initTracker(funcLoc);
		});
	},

	getSubFuncLocs: function getSubFuncLocsFn(funcLoc) {
		console.log(funcLoc);
		obj.tplnr = funcLoc;

		//		Anzeige der Outline
		var dummy = {Functlocation : funcLoc };
		this.showOutLine( dummy );

		// set busy indicator visible
		var busy = sap.ui.getCore().byId('flBusy');
		busy.setVisible(true);
		busy.setBusy(true);

		// timeout for seeing loading screen
		setTimeout(function() {
			// set mocked data
			setDataset();

			obj.subFuncLocLoad = true;
			sap.ui.getCore().byId('initDetails--snapCancel').setVisible(true);
			// add event to detail button
			sap.ui.getCore().byId('application-MasterBtn').setVisible(true);
			document.getElementById('initDetails--initPage-title').innerHTML = obj.tplnr;
			console.log( obj.tplnr );
			sap.ui.getCore().byId("initDetails").getController().onFunclocRecognized( obj.data );
		}, 2000);
	}
};


crazyArchitectWorld.init();