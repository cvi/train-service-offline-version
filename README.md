# README - Train Service Offline Version #

### What is this repository for? ###

Mobile application for the maintenance of trains with augmented reality support. Application is based on Apache Cordova and works platform independent. 
Used frontend technologies are SAPUI5 and JQuery. Data Consumption through OData services. The augmented reality functionality is supported by Wikitude. 
The application contains following features:

* Track train to load equipments and parts
* Once a train is tracked show additional information (operation hours, weight, etc.)
* Equipment Overview
* Visualise equipments as 3D Objects (Augmented Reality)
* Record notifications for broken parts
* Show videos for part exchange (Augmented Reality)

### How to use the app ###

Once the App is running on your device, wait until the camera view has completely loaded. Then hold your device in direction of a trackable vehicle. 
All augmented reality features need that the vehicle is tracked again to trigger the visualisation.

### How do I get set up? ###

Use this script for creating new project based on the Helicopter Service AR Application (Windows Bash):


```
#!Bash

:: get the www folder from the git
RMDIR /S localGitFiles
git clone %GIT_WITH_WWW_FOLDER% %WWW_FolderName%

:: create the new app
CALL cordova create %AppName% com.orianda.%AppName% %AppName%

:: go into the project directory
CD %AppName%

:: add platform
CALL cordova platform add android
CALL cordova plugin add https://github.com/Wikitude/wikitude-phonegap.git -d 
CALL cordova plugin add https://github.com/lampaa/com.lampa.startapp.git -d

:: COPY %WWW_FolderName% www/
XCOPY /S %WWW_FolderName% www\

:: copy the config and image files
COPY %config_Folder%
XCOPY /S %res_FolderName% res\
CALL cordova build
```


Use this script for creating new project based on the Helicopter Service AR Application (IOS Bash):

```
#!sh

#!/bin/sh

#config
AppName='CoILAR'
WWW_FolderName='localGitFiles' 
GIT_WITH_WWW_FOLDER='https://bkr@bitbucket.org/cvi/coilar.git'

# -- script
BASEDIR=$(dirname $0)

# get the www folder from the git
rm -rf ${BASEDIR}/${WWW_FolderName}
git clone ${GIT_WITH_WWW_FOLDER} ${BASEDIR}/${WWW_FolderName}

# update the system
sudo npm update -g cordova

# create the new app
cordova create ${BASEDIR}/${AppName} com.orianda.${AppName} ${AppName}

# go into the project directory
cd ${BASEDIR}/${AppName}

# add platform
 cordova platform add ios

# add plugins
cordova plugin add https://github.com/Wikitude/wikitude-phonegap.git -d 
cordova plugin add ${BASEDIR}/KapselSDK/plugins/logon

# copy the www file into the project
cp -rf ${BASEDIR}/${WWW_FolderName}/ www/

# copy the config and image files
cp -rf ${BASEDIR}/config.xml .
cp -rf ${BASEDIR}/res res

cordova prepare ios
#cordova compile ios # xcode will do it for us

#open xcode
open ${BASEDIR}/${AppName}/platforms/ios/${AppName}.xcodeproj
```