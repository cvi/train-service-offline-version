sap.ui.controller("program-resources.controller.initDetails", {

	/**
	 * function for taking snapshot (HuD) from AR-target
	 */
	cancelSnapShot: function(oEvent) {
		document.getElementById('initDetails--initPage-title').innerHTML = "Track a Functional Location...";
		sap.ui.getCore().byId('application-MasterBtn').setVisible(false);
		sap.ui.getCore().byId('initDetails--snapCancel').setVisible(false);
		sap.ui.getCore().byId('createButton').setVisible(false);
		oSplitApp.backToTopMaster();
		// explicitly hide Master
		oSplitApp.setMode(sap.m.SplitAppMode.HideMode);
		oSplitApp.hideMaster();
		crazyArchitectWorld.removeModel(obj.data.d);
		crazyArchitectWorld.removeOutLine(obj.data.d);
		$("#tiles").hide();
		// initialize object
		obj.data = null;
	},

	/**
	 * function called when image recognized
	 * @param
	 */
	onFunclocRecognized: function(data) {
		// some business data
		for(var i=0; i<data.d.MesPnts.results.length; i++){
			try{
				if( this.byId(data.d.MesPnts.results[i].Psort) != null){
					data.d.MesPnts.results[i].MesDocs.results.forEach(function(MesDoc){
						//Float parsen
						MesDoc.Cntrr = parseFloat(MesDoc.Cntrr);
						//Konvertierung Datum
						MesDoc.createDate = convertDate(MesDoc.Erdat.split("/")) + " " + convertTime(MesDoc.Eruhr.substring(2, MesDoc.Eruhr.length-3));
						//Konvertierung von ms -> h
						if( MesDoc.Recdu == 'H'){
							MesDoc.Cntrr = MesDoc.Cntrr / 3600;
						}
					});
					var oModel = new sap.ui.model.json.JSONModel(data.d.MesPnts.results[i]);

				    // A Dataset defines how the model data is mapped to the chart
					var oDataset = new sap.viz.ui5.data.FlattenedDataset({
						dimensions : [{
							axis : 1, // must be one for the x-axis, 2 for y-axis
							name : 'Date',
							value : "{createDate}"
						}],
						// it can show multiple measures, each results in a new set of bars in a new color
						measures : [{
							name : data.d.MesPnts.results[i].Pttxt, // 'name' is used as label in the Legend
							value : '{Cntrr}' // 'value' defines the binding for the displayed value
						}],
						// 'data' is used to bind the whole data collection that is to be displayed in the chart
						data : {
							path : "/MesDocs/results"
						}
					});
				    // create a column chart
					this.byId(data.d.MesPnts.results[i].Psort).setDataset(oDataset);
					// attach the model to the chart and display it
					this.byId(data.d.MesPnts.results[i].Psort).setModel(oModel);
				}
			}catch(err){}
		}
		this.byId("aircraftCms2").setValue(data.d.HeliCms2);
		this.byId("aircraftCms3").setValue(data.d.HeliCms3);
		this.byId("aircraftAtwe").setValue(data.d.HeliAtwe);
		this.byId("aircraftCms4").setValue(data.d.maxSpeed);

		//Installed Options
		var installedOptions = new Array( );
		data.d.HeliConc.split(";").forEach( function(option){
			if( option != ""){
				installedOptions.push({"installedOption" : option });
			}
		});
		var oModel = new sap.ui.model.json.JSONModel();

			// set data to created model and bind it to the list
		oModel.setData(installedOptions);
		this.byId("installedOptions").setModel(oModel);

		// create listitems and define shown entries
		var listItems =  new sap.m.StandardListItem({title: "{installedOption}"});
			// define path to list in json object
		this.byId("installedOptions").bindItems("/", listItems);

		this.byId("planningInformation").setText(data.d.Planplant);
		this.byId("objectInformation").setText(data.d.Functlocation + " " + data.d.Descript);
		this.byId("heliApplication").setText(data.d.HeliApplication);
		this.byId("immatriculationNo").setText(data.d.HeliImma);

		this.byId("numberOfNotifications").setValue(data.d.Notifs.results.length);
		this.byId("numberOfOrders").setValue(data.d.Orders.results.length);
		this.byId("initPage").setTitle(obj.tplnr);
		console.log("Set Title" + obj.tplnr);

		setTimeout(function() {
			// load tiles when subfunclocs loaded
			if(!obj.reload) {
				$("#tiles").show();
			}
			// hide busy indicator
			var busy = sap.ui.getCore().byId('flBusy');
			busy.setVisible(false);
			busy.setBusy(false);
		}, 1500);
	},

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf mvc.MVC
*/
	onInit: function() {
		// delegate before show event
		this.getView().addDelegate({
			onBeforeShow: function(evt) {
				sap.ui.getCore().byId("createButton").setVisible(false);
				oSplitApp.setMode(sap.m.SplitAppMode.HideMode);
				// remove model if funcloc tracked and dataset not empty
				if( obj.data != null && obj.tplnr != null ){
					crazyArchitectWorld.removeModel(obj.data.d);
				}
				// check if a reload was triggered after notif creation
				if(obj.reload) {
        			sap.ui.getCore().byId("initDetails").getController().onFunclocRecognized( obj.data );
        		}
			}
		});
        // delegate after show event
		this.getView().addDelegate({
        	onAfterShow: function(evt) {
        		// make detail view transparent
        		$("#bodyID").css({ 'background' : 'transparent' });
        		$("#application").css({ "background" : "transparent" });
        		$("#application-BG").css({ "opacity": '0' });
        		$("#application-Detail").css({ "background" : "transparent" });
        		$("#initDetails").css({ "background" : "transparent" });
        		$("#__page4").css({ "background" : "transparent" });
				// show tiles if funcloc was tracked and dataset not empty
        		if( obj.data != null && obj.tplnr != null){
					$("#tiles").show();
					crazyArchitectWorld.removeModel( obj.data.d );
				}
        		// check if a reload was triggered after notif creation
				if(obj.reload) {
        			document.getElementById('initDetails--initPage-title').innerHTML = obj.tplnr;
        			sap.ui.getCore().byId("initDetails").getController().onFunclocRecognized( obj.data );
        			// show tiles after loading informations
        			$("#tiles").show();
					crazyArchitectWorld.removeModel( obj.data.d );
        			obj.reload = false;
        		}
        	}
        });
	}
});