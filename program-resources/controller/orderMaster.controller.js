// Define controller object - includes object functions
sap.ui.controller("program-resources.controller.orderMaster", {
	
	/*
	 * function for filling order list
	 */
	oOrderFillList: function() {
		var dataset = obj.data.d.Orders;
		var view = this.getView();
		var oModel = new sap.ui.model.json.JSONModel();
		
		// set data to created model and bind it to the list
		oModel.setData(dataset);
		obj.orderList = view.byId("orderList").setModel(oModel);
		
		// create listitems and define shown entries
		var listItems =  new sap.m.StandardListItem({
			title: "{Orderid}",
			description: "{ShortText}",
			press: [this.onOrderSelect, this]
		});

		// define path to list in json object
		obj.orderList.bindItems("/results", listItems);
	},
	
	/*
	 * function for handling order listelement click
	 */
	onOrderSelect: function(oEvent) {
		obj.selPath = oEvent.oSource._aSelectedPaths[0];
		sap.ui.getCore().byId("orderDetails").getController().oFillForm(obj.data.d.Orders);
 		// go to next view
		oSplitApp.toDetail("orderDetails");
		oSplitApp.hideMaster( );
		sap.ui.getCore().byId("__page3-navButton").setVisible(false);
		
		// called when document button selected
		if($('#orderDetails--documentButton').hasClass('sapMITBSelected')) {
			oDocButtonClicked();
		}
	},

	/*
	 * function for searching with searchfield
	 */
	onSearch: function(oEvent) {
		console.log("searchField: search for: " + oEvent.getParameter("query"));
//		var oFilterEqunr = new sap.ui.model.Filter("Ordernr", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("newValue"));
		var oFilterShtxt = new sap.ui.model.Filter("Oshtxt", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("query"));
		var oListBinding = obj.orderList.getBinding("items");
		oListBinding.filter([oFilterShtxt]);
	},
	
	/*
	 * function for livechange of searchfield
	 */
	onLiveChange: function(oEvent) {
		console.log("searchField: liveChange for: "	+ oEvent.getParameter("newValue"));
//		var oFilterEqunr = new sap.ui.model.Filter("Ordernr", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("newValue"));
		var oFilterShtxt = new sap.ui.model.Filter("Oshtxt", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("newValue"));
		var oListBinding = obj.orderList.getBinding("items");
		oListBinding.filter([oFilterShtxt]);
	},
	
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf mvc.MVC
*/
//	onInit: function() {
//
//	}

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf mvc.MVC
*/
//	onBeforeRendering: function() {
//		
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf mvc.MVC
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf mvc.MVC
*/
//	onExit: function() {
//
//	}

});