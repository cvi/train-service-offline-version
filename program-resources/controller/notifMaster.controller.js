
// Define controller object - includes object functions
sap.ui.controller("program-resources.controller.notifMaster", {

	oNotifFillList: function() {
		// loop through object
		for(var i = 0; i<obj.data.d.Notifs.results.length; i++) {
			var oArray = obj.data.d.Notifs.results[i].Qmtxt.split(":");
			if(oArray.length == 2) {
				obj.data.d.Notifs.results[i].Eqname = oArray[0];
				obj.data.d.Notifs.results[i].Qmtxt = oArray[1];
			}
		}

		var dataset = obj.data.d.Notifs;
		var view = this.getView();
		var oModel = new sap.ui.model.json.JSONModel();

		// set data to created model and bind it to the list
		oModel.setData(dataset);
		obj.notifList = view.byId("notifList").setModel(oModel);

		// create listitems and define shown entries
		var listItems =  new sap.m.StandardListItem({
			title: "{Qmnum}",
			description: "{Qmtxt}",
			press: [this.onNotifSelect, this]
		});

		// define path to list in json object
		obj.notifList.bindItems("/results", listItems);
	},

	/*
	 * Function for handling listlement selection
	 */
	onNotifSelect: function(oEvent) {
		obj.selPath = oEvent.oSource._aSelectedPaths[0];
		sap.ui.getCore().byId("notifDetails").getController().oFillForm(obj.data.d.Notifs);
 		// go to next view
		oSplitApp.toDetail("notifDetails");
		sap.ui.getCore().byId("__page2-navButton").setVisible(false);
	},

	/*
	 * Function for handling detailsButton press
	 */
	detailButtonPress: function(oEvent) {
		alert("detailsbutton clicked");
	},

	/*
	 * function for searching with searchfield
	 */
	onSearch: function(oEvent) {
		console.log("searchField: search for: " + oEvent.getParameter("query"));
		var oFilterShtxt = new sap.ui.model.Filter("Qmtxt", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("query"));
		var oListBinding = obj.notifList.getBinding("items");
		oListBinding.filter([oFilterShtxt]);
	},

	/*
	 * function for livechange of searchfield
	 */
	onLiveChange: function(oEvent) {
		console.log("searchField: liveChange for: "	+ oEvent.getParameter("newValue"));
		var oFilterShtxt = new sap.ui.model.Filter("Qmtxt", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("newValue"));
		var oListBinding = obj.notifList.getBinding("items");
		oListBinding.filter([oFilterShtxt]);
	},
});