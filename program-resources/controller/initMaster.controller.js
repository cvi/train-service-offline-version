sap.ui.controller("program-resources.controller.initMaster", {
	
	/*
	 * function called when item for notification creation is clicked
	 */
	onNotifCreateItemTap: function(oEvent) {	
		// fill list with equipments
		sap.ui.getCore().byId("equipMaster").getController().equipFillList();		
		// go to next view
		var oSplitApp = sap.ui.getCore().byId("application");
		oSplitApp.toMaster("equipMaster");
	},	
	
	/*
	 * function called when item for notification listing is clicked
	 */
	onNotifListItemTap: function(oEvent) {
		// fill list with Notifications
		sap.ui.getCore().byId("notifMaster").getController().oNotifFillList();
		// go to next view
		var oSplitApp = sap.ui.getCore().byId("application");
		oSplitApp.toMaster("notifMaster");
	},
	
	/*
	 * function called when item for showing 3D-ETK is clicked
	 */
	onEtkListItemTap: function(oEvent) {
		document.location = 'architectsdk://action';
	},
	
	/*
	 * 
	 */
	onOrderListItemTap: function(oEvent) {
		// fill list with Orders
		sap.ui.getCore().byId("orderMaster").getController().oOrderFillList();
		// go to next view
		var oSplitApp = sap.ui.getCore().byId("application");
		oSplitApp.toMaster("orderMaster");
	},
});