sap.ui.jsview("program-resources.views.initMaster", {

	/** Specifies the Controller belonging to this View.
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/
	getControllerName : function() {
		return "program-resources.controller.initMaster";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	* Since the Controller is given to this method, its event handlers can be attached right away.
	* @memberOf mvc.MVC
	*/
	createContent : function(oController) {
		// get Icon Names
		jQuery.sap.require("sap.ui.core.IconPool");
		var aNames = sap.ui.core.IconPool.getIconNames();

		// create task list: Notification
		var oUpperTaskList = new sap.m.List({
			inset: true,
			headerText: "Notification",
			items: [
			        new sap.m.StandardListItem(this.createId('liCreate'), {
			        	title : "Create",
			        	icon: sap.ui.core.IconPool.getIconURI(aNames[55]),
			        	type : sap.m.ListType.Active,
			        	press : [oController.onNotifCreateItemTap, oController]
			        }),
			        new sap.m.StandardListItem(this.createId('liList'), {
			        	title : "List",
			        	icon: sap.ui.core.IconPool.getIconURI(aNames[9]),
			        	type : sap.m.ListType.Active,
			        	press : [oController.onNotifListItemTap, oController]
			        })
		    ]
		});

		// create task list: Maintenance Order
		var oMiddleTaskList = new sap.m.List({
			inset: true,
			headerText: "Maintenance Order",
			items: [
			        new sap.m.StandardListItem(this.createId('liListOrder'), {
			        	title: "List",
			        	icon: sap.ui.core.IconPool.getIconURI(aNames[9]),
			        	type : sap.m.ListType.Active,
			        	press : [oController.onOrderListItemTap, oController]
			        })
			]
		});

		// create task list: Parts
		var oUnderTaskList = new sap.m.List({
			inset: true,
			headerText: "Parts",
			items: [
			        new sap.m.StandardListItem(this.createId('li3DETK'), {
			        	title: "3D-ETK",
			        	icon: sap.ui.core.IconPool.getIconURI(aNames[12]),
			        	type : sap.m.ListType.Active,
			        	press : [oController.onEtkListItemTap, oController]
			        })
			]
		});

		// Wrapper for Tasklistitems
		var TaskBox = new sap.m.FlexBox({
			items: [
			        oUpperTaskList,
			        oMiddleTaskList,
			        oUnderTaskList
		    ],
		    direction : "Column"
		});

		// page footer
		var pageFooter = new sap.m.Bar({});


		return new sap.m.Page({
			title: "Tasks",
			content: TaskBox,
			footer: pageFooter
		});

	}

});