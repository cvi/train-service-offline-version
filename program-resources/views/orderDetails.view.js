sap.ui.jsview("program-resources.views.orderDetails", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/ 
	getControllerName : function() {
		return "program-resources.controller.orderDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf mvc.MVC
	*/ 
	createContent : function(oController) {
		
		/* ################################################################################################################
		 * 												Order Object Header
		 * ################################################################################################################
		 */
		var oOrderObjectHeader = new sap.m.ObjectHeader(this.createId("orderObjectHeader"), {});

	
		/* ################################################################################################################
		 * 											Order detail informations
		 * ################################################################################################################
		 */
	
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Order Formular Elements
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		// formular title
		var oOrderFormularTitle = new sap.m.FlexBox({
			items : [
			    new sap.m.Text({
			    	text : "Order information", width : "25em",
			    	textAlign : sap.ui.core.TextAlign.Left
			    })]});
		
		// formular
		var oOrderFormular = new sap.m.FlexBox(this.createId("orderFormContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
						new sap.m.Label({
							text : "Order type:", width : "15em",
							layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
						}),
						new sap.m.Text(this.createId("orderTypeText"), {
							width : "20em"
						})
			    	]}),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Maintenance type:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("orderMaintTypeText"), {
		 			    	width : "20em"
		 			    })
	 			    ]}),
			    new sap.m.FlexBox({
			    	items : [
		 			   new sap.m.Label({
		 				   	text : "Description:", width : "15em",
		 				   	layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
		 			   }),
		 			   new sap.m.Text(this.createId("orderDescText"), {
		 				    width : "20em"
		 			   })
		    	    ]
			    })
        	],
			direction : "Column",
			alignItems : "Center"
		});
		
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Order Information Elements
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		
		// Information title
		var oOrderObjectInformationTitle = new sap.m.FlexBox({
			items : [
			    new sap.m.Text({
			    	text : "Object information", width : "25em",
			    	textAlign : sap.ui.core.TextAlign.Left
			    })]});
		
		// Information
		var oObjectInformations = new sap.m.FlexBox(this.createId("objectInformationContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "FL-Indicator:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("objectTplnrText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Equipment Indicator:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("objectEqunrText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    })
			],
			direction : "Column",
			alignItems : "Center"
		});
		
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Deadline Elements
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		
		// Deadline title
		var oOrderDeadlineTitle = new sap.m.FlexBox({
			items : [
			    new sap.m.Text({
			    	text : "Deadlines", width : "25em",
			    	textAlign : sap.ui.core.TextAlign.Left
			    })]});
		
		// Deadlines
		var oDeadlines = new sap.m.FlexBox(this.createId("objectDeadlineContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Bsc start:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("deadlineStartText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Bsc finish:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("deadlineEndText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    })
			],
			direction : "Column",
			alignItems : "Center"
		});
		
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Deadline Elements
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		
		// Responsability title
		var oOrderResponTitle = new sap.m.FlexBox({
			items : [
			    new sap.m.Text({
			    	text : "Responsabilities", width : "25em",
			    	textAlign : sap.ui.core.TextAlign.Left
			    })]});
		
		// Responsability
		var oResponsability = new sap.m.FlexBox(this.createId("objectResponContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Main Work Ctr:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("responText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    })
			],
			direction : "Column",
			alignItems : "Center"
		});
		
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Partner Elements
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		
		// Partner title
		var oOrderPartnerTitle = new sap.m.FlexBox({
			items : [
			    new sap.m.Text({
			    	text : "Partner", width : "25em",
			    	textAlign : sap.ui.core.TextAlign.Left
			    })]});
		
		// Partner
		var oPartnerData = new sap.m.FlexBox(this.createId("objectPartnerContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Partner information:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("partnerText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    })
			],
			direction : "Column",
			alignItems : "Center"
		});
		
		
		/* ################################################################################################################
		 * 											Material informations
		 * ################################################################################################################
		 */
		
		// table header
		var oMaterialTableHeader = new sap.m.Toolbar({
			content : [
			           new sap.m.Label(this.createId("materialTableItemslabel"), {text : "Elemente (0)"}),
			           new sap.m.ToolbarSpacer(),
			           ]});
		
		// table info
		var oMaterialTableInfo = new sap.m.Toolbar(this.createId("materialTableInfo"), {
			content : [
			           new sap.m.Label({text : "List with all available materials"})
			           ]});
		
		// Materials
		var oMaterialTable = new sap.m.Table(this.createId("materialTable"), {
			headerToolbar: oMaterialTableHeader,
			infoToolbar : oMaterialTableInfo,
			columns : [
			           new sap.m.Column({
			        	   header : new sap.m.Label({text : "Material Nr."})
			           }),
			           new sap.m.Column({
			        	   header : new sap.m.Label({text : "Description"})
			           }),
			           new sap.m.Column({
			        	   header : new sap.m.Label({text : "Quantity"})
			           }),
			           new sap.m.Column({
			        	   header : new sap.m.Label({text : "Unit"})
			           }),
			]
		});
		
		
		/* ################################################################################################################
		 * 											Attachment informations
		 * ################################################################################################################
		 */
		
		// attachment dummy
//		var oAttachment = new sap.m.FlexBox(this.createId("attachmentContainer"), {
//			items : [ 
//			    new sap.m.Image("dummyPic2", {
//				    src: "http://upload.wikimedia.org/wikipedia/commons/d/dd/Marenco_SKYe_SH09_prototype_(1).jpg",
//				    width: "80%", height: "80%",
//				    layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
//			    })
//			],
//			direction : "Column",
//			alignItems : "Center"
//	    });
		
		/* ################################################################################################################
		 * 											Order completion informations
		 * ################################################################################################################
		 */		
		
		// Completion title
		var oOrderCompletionTitle = new sap.m.FlexBox({
			items : [
			    new sap.m.Text({
			    	text : "Completion Informations", width : "25em",
			    	textAlign : sap.ui.core.TextAlign.Left
			    })]});
		
		
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Completion Information
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		var oCompletion = new sap.m.FlexBox(this.createId("objectCompletionContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Process Nr:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("processNrText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Process Description:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("processShtxtText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Completion Nr:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("completionNrText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Staff Nr.:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("staffNrText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Completing Person:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("staffText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Duration:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("durationText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Completion Text:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("completionText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Date work start:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("startDateText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Time work start:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("startTimeText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Date work end:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("endDateText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			    new sap.m.FlexBox({
			    	items : [
	 			    	new sap.m.Label({
	 			    		text : "Time work end:", width : "15em",
	 			    		layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})	
	 			    	}),
		 			    new sap.m.Text(this.createId("endTimeText"), {
		 			    	width : "20em"
		 			    })
	 			    ]
			    }),
			],
			direction : "Column",
			alignItems : "Center"
		});
		
		/*-----------------------------------------------------------------------------------------------------------------
		 *  										Completion Button
		 *----------------------------------------------------------------------------------------------------------------- 
		 */
		
		var oSubCompletion = new sap.m.FlexBox(this.createId("completionButtonContainer"), {
			items : [
			    new sap.m.FlexBox({
			    	items : [
			    	    new sap.m.Label({
			    	    	text : "Technical Completion", width : "15em",
			    	    	layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
			    	    }),
			    	    new sap.m.FlexBox({
			    	    	items : [
				    	    	new sap.m.Switch(this.createId("completionSwitch"), {
									change: [oController.oOrderCompletion, oController],
									type : sap.m.SwitchType.AcceptReject
								})
							],
							width : "17.5em"
			    	    })
	 			    ]
			    })
			],
			direction : "Column",
			alignItems : "Center"
		});
		

		/* ################################################################################################################
		 * 											Tab container
		 * ################################################################################################################
		 */
		
		var oOrderTabContainer = new sap.m.IconTabBar(this.createId("orderIconTabBar"),{
			items : [
				new sap.m.IconTabFilter(this.createId("detailButton"), {
					icon : "sap-icon://hint",
					text : "Detail",
					key  : "Detail",
					content : [ oOrderFormularTitle, oOrderFormular, oOrderObjectInformationTitle, oObjectInformations,
					            oOrderDeadlineTitle, oDeadlines, oOrderResponTitle, oResponsability,
					            oOrderPartnerTitle, oPartnerData ]
				}), new sap.m.IconTabFilter(this.createId("materialButton"), {
					icon : "sap-icon://inventory",
					text : "Material",
					key  : "Material",
					content : [ oMaterialTable ]
				}),
				new sap.m.IconTabFilter(this.createId("documentButton"), {
					icon : "sap-icon://attachment",
					text : "Documents",
					key  : "Documents",
//					content : [ oAttachment ]
				}),
				new sap.m.IconTabFilter(this.createId("completionButton"), {
					icon : "sap-icon://customer-order-entry",
					text : "Completion",
					key  : "Completion",
					content : [ oOrderCompletionTitle, oCompletion, oSubCompletion] //, oSubCompletion 
				}),
			]
		});

		// create button for page footer
		var cancelButton = new sap.m.Button(this.createId("orderCancelButton"), {
			icon : "sap-icon://decline",
			visible : {
				path : "local>/inEdit",
				formatter : function(inEdit) { return !inEdit; }
			},
			press : oController.oCancelOrder
		});
		
		// page footer
		var pageFooter = new sap.m.Bar({contentRight : [cancelButton]});
		
		var oTabContent = new sap.m.FlexBox({
			items : [oOrderObjectHeader, oOrderTabContainer],
			direction : "Column"
		});
		
		// Include all flexboxes into page element #e6f2f9
		return new sap.m.Page({
			title: "Acknowledgement",
			showNavButton : false,
			content: oTabContent,
			footer : pageFooter
		});	
	}
});